<?php

	require_once 'src/database.php';

	runQuery('DROP TABLE IF EXISTS users');
	runQuery('DROP TABLE IF EXISTS tweets');

	// Truncate Table Alternative: 
	// runQuery('TRUNCATE TABLE users');
	// runQuery('TRUNCATE TABLE tweets');

$createTweets = "CREATE TABLE IF NOT EXISTS `tweets` (
  `id` int(10) unsigned NOT NULL,
  `text` varchar(255) NOT NULL,
  `datetime` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=latin1;";


$createUsers = "CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `handle` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `real_name` varchar(255) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;";

	echo 'Database Reset.';
	runQuery($createTweets);
	runQuery($createUsers);

?>

