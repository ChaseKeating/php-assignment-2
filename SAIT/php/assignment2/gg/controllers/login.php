<?php

require 'src/functions.php';

if (formWasSubmitted()) {
    if (correctEmail() && correctPassword()) {
        redirectTo('main.php');
    } else {
        if (!correctEmail()) {
            echo 'ERROR! Incorrect Email.';
        }
        if (!correctPassword()) {
            echo 'ERROR! Incorrect Password.';
        }
    }
}

require 'views/login.php';
