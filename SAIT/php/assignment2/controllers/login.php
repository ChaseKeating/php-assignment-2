<?php

session_start();

require 'src/functions.php';

if (checkSession()) {
    echo 'You are logged in';
 } else {
 	echo 'Please log in';
 }

if (formWasSubmitted()) {
    $loginEmail = $_POST['email'];
	$loginName = $_POST['name'];
	$loginPassword = $_POST['password'];
	$securePassword = mysqli_real_escape_string($loginPassword);
	$encryptedPassword = password_hash($securePassword, PASSWORD_DEFAULT); 
  loginUser();
}

require 'views/login.php';
