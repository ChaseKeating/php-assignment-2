<?php


 if( isset($_SESSION['user'])!="" ){ // CHECK FOR USER SESSION
  header("Location: home.php");
 }

//    REGISTRATION PAGE    //
//    CHASE KEATING 2016   //

require 'src/config.php';
require 'src/functions.php';
require 'views/registration.php';


if (formWasSubmitted()) {

    $hashingOptions = [
        'cost' => 10,
    ];
 
	$unencryptedPassword = htmlspecialchars($_POST['password']);
	$password = password_hash($unencryptedPassword, PASSWORD_DEFAULT, $hashingOptions);

    if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{4,12}$/', $unencryptedPassword)) {
        echo "Your password is not complex enough.";
    }
    else {
    	registerUser();
    	header('Location: main.php');

    }
}



// 



function runQuery($sql)
{
    $db = new mysqli('localhost', 'wbdv', 'phpisawesome', 'wbdv2016');
    $result = $db->query($sql);

    if ($result == false) {
        echo '<pre>';
        var_dump('VERY BAD ERROR', $db->error);
        die;
    }
}

// 
