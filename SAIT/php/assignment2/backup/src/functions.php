<?php

function redirectTo($url)
{
    header('Location: '.$url);
}

function formWasSubmitted()
{
    return !empty($_POST);
}

function checkLength($fieldname, $length)
{
    return !empty($_POST[$fieldname]);

}

function fieldIsTooLong($fieldName, $length)
{
    return strlen($_POST[$fieldName]) < $length;
}