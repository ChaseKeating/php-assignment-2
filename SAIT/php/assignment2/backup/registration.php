<?php

require 'src/functions.php';
require 'src/db.php';
require 'views/registration.php';

$password = $_POST['password'];
$uppercase = preg_match('@[A-Z]@', $password);
$lowercase = preg_match('@[a-z]@', $password);
$number    = preg_match('@[0-9]@', $password);
?>

<?php 

if(!$uppercase || !$lowercase || !$number || strlen($password) < 8) {
  echo '<p>Your password is not complex enough.</p>';
}
?>



