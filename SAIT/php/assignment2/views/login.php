<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  <link href='./css/style.css' rel='stylesheet' type='text/css'>
  <title>Login Form</title>
</head>
<body>
<div class="login">
<h2 class="active">sign in</h2>
<a href="registration.php"><h2 class="nonactive">sign up</h2></a>
  <form method="post">
    <input type="text" class="text" name="name"/>
    <span>Name</span>
    <br><br>
    <input type="text" class="text" name="email"/>
    <span>Email</span>
    <br><br>
    <input type="text" class="text" name="password" />
    <span>Password</span>
    <br><br>
    <button type="submit" class="signin">Submit</button>
</form>
</body>
</html>
